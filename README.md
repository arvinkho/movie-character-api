# Background
This is a console Java application for the third assignment in the Accelerate course.

It is an intro to Hibernate and PostgreSQL. The project is dockerized and pushed to Heroku: https://ak-movie-characters-api.herokuapp.com

It covers some design patterns and principles, such as the Repository-pattern, service-repository pattern and DTO-pattern.

# Install
* Install JDK 17
* Install IntelliJ
* Clone repo

# Usage
The three paths to the api is /api/v1/movies, /api/v1/characters and /api/v1/franchises. For more information, visit https://ak-movie-characters-api.herokuapp.com/swagger-ui/index.html

This application is an api for a movie character database. It can **C**reate, **R**ead, **U**pdate and **D**elete entries, such as
movies, franchises and moviecharacters. You can also read which movies and characters are present in a specific franchise,
which franchise a movie is a part of and characters present in movies. Read the Api documentation for more detailed information:
https://ak-movie-characters-api.herokuapp.com/swagger-ui/index.html

