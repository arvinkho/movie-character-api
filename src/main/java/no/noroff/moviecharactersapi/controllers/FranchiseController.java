package no.noroff.moviecharactersapi.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.moviecharactersapi.models.domain.Franchise;
import no.noroff.moviecharactersapi.models.dto.franchise.CreateFranchiseDto;
import no.noroff.moviecharactersapi.models.dto.franchise.FranchiseDto;
import no.noroff.moviecharactersapi.models.dto.movie.CreateMovieDto;
import no.noroff.moviecharactersapi.models.dto.movie.MovieDto;
import no.noroff.moviecharactersapi.models.dto.moviecharacter.MovieCharacterDto;
import no.noroff.moviecharactersapi.models.mappers.FranchiseMapper;
import no.noroff.moviecharactersapi.models.mappers.MovieCharacterMapper;
import no.noroff.moviecharactersapi.models.mappers.MovieMapper;
import no.noroff.moviecharactersapi.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/v1/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseService franchiseService;
    @Autowired
    private FranchiseMapper franchiseMapper;
    @Autowired
    private MovieMapper movieMapper;
    @Autowired
    private MovieCharacterMapper movieCharacterMapper;

    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "found all franchises",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDto.class))}),
            @ApiResponse(responseCode = "204", description = "no franchises exists")
    })
    @GetMapping
    public ResponseEntity<List<FranchiseDto>> getAll() {
        HttpStatus status;
        List<FranchiseDto> franchiseDtos = new ArrayList<>();
        if (franchiseService.count() == 0){
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.OK;
            franchiseDtos = franchiseMapper.mapFromEntityToDtoList(franchiseService.getAll());
        }
        return new ResponseEntity<>(franchiseDtos, status);
    }

    @Operation(summary = "Get single franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "found the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDto.class))}),
            @ApiResponse(responseCode = "404", description = "no franchise found by the id")
    })
    @GetMapping("{id}")
    public ResponseEntity<FranchiseDto> getById(@PathVariable int id) {
        HttpStatus status;
        FranchiseDto franchiseDto = null;
        if (!franchiseService.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
        } else {
            status = HttpStatus.OK;
            franchiseDto = franchiseMapper.mapFromEntityToDto(franchiseService.getById(id));
        }
        return new ResponseEntity<>(franchiseDto, status);
    }

    @Operation(summary = "Get all movies in a franchise by franchise id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "found all movies in the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDto.class))}),
            @ApiResponse(responseCode = "204", description = "no movies in the franchise"),
            @ApiResponse(responseCode = "400", description = "no franchise with the franchise id exists")
    })
    @GetMapping("{id}/movies")
    public ResponseEntity<Set<MovieDto>> getMoviesInFranchise(@PathVariable int id) {
        HttpStatus status;
        Set<MovieDto> movies = new HashSet<>();
        if (!franchiseService.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            movies = movieMapper.mapFromEntityToDtoSet(franchiseService.getAllMoviesById(id));
            if (movies.isEmpty()) {
                status = HttpStatus.NO_CONTENT;
            } else {
                status = HttpStatus.OK;
            }
        }
        return new ResponseEntity<>(movies, status);
    }

    @Operation(summary = "Get all characters in a franchise by franchise id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "found all characters in the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacterDto.class))}),
            @ApiResponse(responseCode = "204", description = "no characters in the franchise"),
            @ApiResponse(responseCode = "400", description = "no franchise with the id exists")
    })
    @GetMapping("{id}/characters")
    public ResponseEntity<Set<MovieCharacterDto>> getCharactersInFranchise(@PathVariable int id) {
        HttpStatus status;
        Set<MovieCharacterDto> characters = new HashSet<>();
        if (!franchiseService.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            characters = movieCharacterMapper.mapFromEntityToDtoSet(franchiseService.getAllMovieCharactersById(id));
            if (!characters.isEmpty()) {
                status = HttpStatus.OK;
            } else {
                status = HttpStatus.NO_CONTENT;
            }
        }
        return new ResponseEntity<>(characters, status);
    }

    @Operation(summary = "Create a new franchise and add to the database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "successfully created a new franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateFranchiseDto.class))})
    })
    @PostMapping
    public ResponseEntity<FranchiseDto> add(@RequestBody CreateFranchiseDto createFranchiseDto) {
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(franchiseMapper.mapFromEntityToDto(
                franchiseService.save(franchiseMapper.mapFromCreateDtoToEntity(createFranchiseDto))), status);
    }

    @Operation(summary = "Update a franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successfully updated the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateFranchiseDto.class))}),
            @ApiResponse(responseCode = "400", description = "no movie with the id exists, or title is too short")
    })
    @PutMapping("{id}")
    public ResponseEntity<FranchiseDto> update(@PathVariable int id, @RequestBody CreateFranchiseDto createFranchiseDto) {
        HttpStatus status;
        FranchiseDto franchiseDto = null;
        if (!franchiseService.existsById(id) || createFranchiseDto.getName().length() < 3) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            status = HttpStatus.OK;
            Franchise franchise = franchiseMapper.mapFromCreateDtoToEntity(createFranchiseDto);
            franchise.setId(id);
            franchiseDto = franchiseMapper.mapFromEntityToDto(franchiseService.save(franchise));
        }

        return new ResponseEntity<>(franchiseDto, status);
    }

    @Operation(summary = "Update movies in a franchise by franchise id and a list of movie ids")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successfully updated the movie ids",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateMovieDto.class))}),
            @ApiResponse(responseCode = "400",
                    description = "no franchise with the id exists, or no supplied movie ids")
    })
    @PutMapping("{id}/movies")
    public ResponseEntity<FranchiseDto> updateMovies(@PathVariable int id, @RequestBody List<Integer> movieIds) {
        HttpStatus status;
        FranchiseDto franchiseDto = null;
        if (!franchiseService.existsById(id) || movieIds.isEmpty()) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            status = HttpStatus.OK;
            franchiseDto = franchiseMapper.mapFromEntityToDto(franchiseService.updateMovies(id, movieIds));
        }
        return new ResponseEntity<>(franchiseDto, status);
    }


    @Operation(summary = "Delete franchise based on franchise id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successfully deleted the franchise",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "304",
                    description = "no changes to the franchise, unsuccessful delete")
    })
    @DeleteMapping("{id}")
    public ResponseEntity<Integer> delete(@PathVariable int id) {
        HttpStatus status;
        franchiseService.delete(id);
        if (franchiseService.existsById(id)) {
            status = HttpStatus.NOT_MODIFIED;
        } else {
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(id, status);
    }
}
