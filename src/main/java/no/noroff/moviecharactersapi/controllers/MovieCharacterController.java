package no.noroff.moviecharactersapi.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.moviecharactersapi.models.dto.movie.CreateMovieDto;
import no.noroff.moviecharactersapi.models.dto.movie.MovieDto;
import no.noroff.moviecharactersapi.models.dto.moviecharacter.CreateMovieCharacterDto;
import no.noroff.moviecharactersapi.models.dto.moviecharacter.MovieCharacterDto;
import no.noroff.moviecharactersapi.models.mappers.MovieCharacterMapper;
import no.noroff.moviecharactersapi.models.domain.MovieCharacter;
import no.noroff.moviecharactersapi.models.mappers.MovieMapper;
import no.noroff.moviecharactersapi.services.MovieCharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/v1/characters")
public class MovieCharacterController {
    @Autowired
    private MovieCharacterService movieCharacterService;
    @Autowired
    private MovieCharacterMapper movieCharacterMapper;
    @Autowired
    private MovieMapper movieMapper;

    @Operation(summary = "Get all movie characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "found all movie characters",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacterDto.class))}),
            @ApiResponse(responseCode = "204", description = "no movie characters exists")
    })
    @GetMapping
    public ResponseEntity<List<MovieCharacterDto>> getAll() {
        HttpStatus status;
        List<MovieCharacterDto> movieCharacterDtos = new ArrayList<>();
        if (movieCharacterService.count() == 0) {
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.OK;
            movieCharacterDtos = movieCharacterMapper.mapFromEntityToDtoList(movieCharacterService.getAll());
        }
        return new ResponseEntity<>(movieCharacterDtos, status);
    }

    @Operation(summary = "Get single movie character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "found the movie character",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacterDto.class))}),
            @ApiResponse(responseCode = "404", description = "no movie character found by the id")
    })
    @GetMapping("{id}")
    public ResponseEntity<MovieCharacterDto> getById(@PathVariable int id) {
        HttpStatus status;
        MovieCharacterDto movieCharacterDto= null;
        if (!movieCharacterService.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
        } else {
            status = HttpStatus.OK;
            movieCharacterDto = movieCharacterMapper.mapFromEntityToDto(movieCharacterService.getById(id));
        }
        return new ResponseEntity<>(movieCharacterDto, status);
    }

    @Operation(summary = "Get all movies a character is present in, by character id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "found all movies the character is playing in",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDto.class))}),
            @ApiResponse(responseCode = "204", description = "character doesn't play in any movies"),
            @ApiResponse(responseCode = "400", description = "no character with the character id exist")
    })
    @GetMapping("{id}/movies")
    public ResponseEntity<Set<MovieDto>> getMoviesFromCharacter(@PathVariable int id) {
        HttpStatus status;
        Set<MovieDto> movies = new HashSet<>();
        if (!movieCharacterService.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            movies = movieMapper.mapFromEntityToDtoSet(movieCharacterService.getMoviesFromMovieCharacter(id));
            if (movies.isEmpty()) {
                status = HttpStatus.NO_CONTENT;
            } else {
                status = HttpStatus.OK;
            }
        }
        return new ResponseEntity<>(movies, status);
    }

    @Operation(summary = "Create a new movie character and add to the database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "successfully created a new movie character",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateMovieCharacterDto.class))})
    })
    @PostMapping
    public ResponseEntity<MovieCharacterDto> add(@RequestBody CreateMovieCharacterDto createMovieCharacterDto) {
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(movieCharacterMapper
                .mapFromEntityToDto(movieCharacterService
                        .save(movieCharacterMapper
                                .mapFromCreateDtoToEntity(createMovieCharacterDto))),
                status);
    }

    @Operation(summary = "Update a movie character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successfully updated the movie character",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateMovieCharacterDto.class))}),
            @ApiResponse(responseCode = "400", description = "no movie character with the id exists, or name is too short")
    })
    @PutMapping("{id}")
    public ResponseEntity<MovieCharacterDto> update(@PathVariable int id, @RequestBody CreateMovieCharacterDto createMovieCharacterDto) {
        HttpStatus status;
        MovieCharacterDto movieCharacterDto = null;
        if (!movieCharacterService.existsById(id) || createMovieCharacterDto.getName().length() < 2) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            status = HttpStatus.OK;
            MovieCharacter movieCharacter = movieCharacterMapper.mapFromCreateDtoToEntity(createMovieCharacterDto);
            movieCharacter.setId(id);
            movieCharacterDto = movieCharacterMapper.mapFromEntityToDto(movieCharacterService.save(movieCharacter));
        }

        return new ResponseEntity<>(movieCharacterDto, status);
    }

    @Operation(summary = "Delete movie character based on movie character id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successfully deleted the movie character",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "304",
                    description = "no changes to the movie character, unsuccessful delete")
    })
    @DeleteMapping("{id}")
    public ResponseEntity<Integer> delete(@PathVariable int id) {
        HttpStatus status;
        movieCharacterService.delete(id);
        if (movieCharacterService.existsById(id)) {
            status = HttpStatus.NOT_MODIFIED;
        } else {
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(id, status);
    }
}
