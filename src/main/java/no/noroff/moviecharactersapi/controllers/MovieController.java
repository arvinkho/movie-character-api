package no.noroff.moviecharactersapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.moviecharactersapi.models.dto.moviecharacter.MovieCharacterDto;
import no.noroff.moviecharactersapi.models.mappers.MovieCharacterMapper;
import no.noroff.moviecharactersapi.models.mappers.MovieMapper;
import no.noroff.moviecharactersapi.models.dto.movie.CreateMovieDto;
import no.noroff.moviecharactersapi.models.dto.movie.MovieDto;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/v1/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;
    @Autowired
    private MovieMapper movieMapper;
    @Autowired
    private MovieCharacterMapper movieCharacterMapper;

    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "found all movies",
            content = {@Content(mediaType = "application/json",
            schema = @Schema(implementation = MovieDto.class))}),
            @ApiResponse(responseCode = "204", description = "no movies exists")
    })
    @GetMapping
    public ResponseEntity<List<MovieDto>> getAll() {
        HttpStatus status;
        List<MovieDto> movieDtos = new ArrayList<>();
        if (movieService.count() == 0){
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.OK;
            movieDtos = movieMapper.mapFromEntityToDtoList(movieService.getAll());
        }
        return new ResponseEntity<>(movieDtos, status);
    }

    @Operation(summary = "Get single movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "found the movie",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDto.class))}),
            @ApiResponse(responseCode = "404", description = "no movie found by the id")
    })
    @GetMapping("{id}")
    public ResponseEntity<MovieDto> getById(@PathVariable int id) {
        HttpStatus status;
        MovieDto movieDto = null;
        if (!movieService.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
        } else {
            status = HttpStatus.OK;
            movieDto = movieMapper.mapFromEntityToDto(movieService.getById(id));
        }
        return new ResponseEntity<>(movieDto, status);
    }

    @Operation(summary = "Get all characters in a movie by movie id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "found all characters in the movie",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacterDto.class))}),
            @ApiResponse(responseCode = "204", description = "no characters in the movie"),
            @ApiResponse(responseCode = "400", description = "no movie with the id exists")
    })
    @GetMapping("{id}/characters")
    public ResponseEntity<Set<MovieCharacterDto>> getCharactersInMovie(@PathVariable int id) {
        HttpStatus status;
        Set<MovieCharacterDto> movieCharacterDtos = new HashSet<>();
        if (!movieService.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            movieCharacterDtos = movieCharacterMapper.mapFromEntityToDtoSet(movieService.getMovieCharactersFromMovie(id));
            if (movieCharacterDtos.isEmpty()) {
                status = HttpStatus.NO_CONTENT;
            } else {
                status = HttpStatus.OK;
            }
        }
        return new ResponseEntity<>(movieCharacterDtos, status);
    }

    @Operation(summary = "Create a new movie and add to the database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "successfully created a new movie",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateMovieDto.class))})
    })
    @PostMapping
    public ResponseEntity<MovieDto> add(@RequestBody CreateMovieDto createMovieDto) {
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(movieMapper
                .mapFromEntityToDto(movieService.save(movieMapper.mapFromCreateDtoToEntity(createMovieDto))),
                status);
    }

    @Operation(summary = "Update a movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successfully updated the movie",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateMovieDto.class))}),
            @ApiResponse(responseCode = "400", description = "no movie with the id exists, or title is too short")
    })
    @PutMapping("{id}")
    public ResponseEntity<MovieDto> update(@PathVariable int id, @RequestBody CreateMovieDto createMovieDto) {
        HttpStatus status;
        MovieDto movieDto = null;
        if (!movieService.existsById(id) || createMovieDto.getTitle().length() < 3) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            status = HttpStatus.OK;
            Movie movie = movieMapper.mapFromCreateDtoToEntity(createMovieDto);
            movie.setId(id);
            movieDto = movieMapper.mapFromEntityToDto(movieService.save(movie));
        }

        return new ResponseEntity<>(movieDto, status);
    }

    @Operation(summary = "Update characters in a movie by movie id and a list of character ids")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successfully updated the movie characters ids",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateMovieDto.class))}),
            @ApiResponse(responseCode = "400",
                    description = "no movie with the id exists, or no supplied character ids")
    })
    @PutMapping("{id}/characters")
    public ResponseEntity<MovieDto> updateCharacters(@PathVariable int id, @RequestBody List<Integer> characterIds) {
        HttpStatus status;
        MovieDto movieDto = null;
        if (!movieService.existsById(id) || characterIds.isEmpty()) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            status = HttpStatus.OK;
            movieDto = movieMapper.mapFromEntityToDto(movieService.updateCharacters(id, characterIds));
        }
        return new ResponseEntity<>(movieDto, status);
    }

    @Operation(summary = "Delete movie based on movie id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successfully deleted the movie",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "304",
                    description = "no changes to the movie, unsuccessful delete")
    })
    @DeleteMapping("{id}")
    public ResponseEntity<Integer> delete(@PathVariable int id) {
        HttpStatus status;
        movieService.delete(id);
        if (movieService.existsById(id)) {
            status = HttpStatus.NOT_MODIFIED;
        } else {
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(id, status);
    }
}
