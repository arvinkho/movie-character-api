package no.noroff.moviecharactersapi.models.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "characters")
public class MovieCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //READONLY???
    private int id;
    @Column(length = 80, nullable = false)
    private String name;
    @Column(length = 80)
    private String alias;
    @Column(length = 10)
    private String gender;
    private String pictureUrl;

    // Relationships
    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;


    public MovieCharacter() {
    }

    public MovieCharacter(int id, String name, String alias, String gender, String pictureUrl, Set<Movie> movies) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.gender = gender;
        this.pictureUrl = pictureUrl;
        this.movies = movies;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Set<Movie> getMovies() {

        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
