package no.noroff.moviecharactersapi.models.dto.franchise;

import java.util.Set;

/**
 * Represents a Data Transfer Object used for CREATION of a new entity.
 */
public class CreateFranchiseDto {

    private String name;
    private String description;

    public CreateFranchiseDto(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
