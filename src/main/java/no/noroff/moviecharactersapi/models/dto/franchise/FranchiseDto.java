package no.noroff.moviecharactersapi.models.dto.franchise;

import no.noroff.moviecharactersapi.models.domain.Franchise;
import no.noroff.moviecharactersapi.models.domain.Movie;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents a Data Transfer Object used for getting an existing entity.
 */
public class FranchiseDto {

    private int id;
    private String name;
    private String description;

    private Set<Integer> movies;

    /**
     * Creates a new franchiseDto object.
     * @param franchise Franchise object to convert to DTO
     */
    public FranchiseDto(Franchise franchise) {
        this.id = franchise.getId();
        this.name = franchise.getName();
        this.description = franchise.getDescription();
        setMovies(franchise.getMovies());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Integer> getMovies() {
        return movies;
    }


    /**
     * Sets the movies of the franchiseDto to a set of integers. If the supplied set of movies is empty,
     * sets the field to null. If not, gets the movie ids from the supplied set and appends to the field set.
     * @param movies set of movies to convert and add.
     */
    public void setMovies(Set<Movie> movies) {
        if (movies == null) {
            this.movies = null;
        } else {
            this.movies = movies.stream().map(Movie::getId).collect(Collectors.toSet());
        }
    }
}
