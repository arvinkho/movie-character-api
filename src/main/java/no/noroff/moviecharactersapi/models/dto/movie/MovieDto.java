package no.noroff.moviecharactersapi.models.dto.movie;

import no.noroff.moviecharactersapi.models.domain.Franchise;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.domain.MovieCharacter;

import java.util.Set;
import java.util.stream.Collectors;


/**
 * Represents a Data Transfer Object used for getting an existing entity.
 */
public class MovieDto {

    private int id;
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String pictureUrl;
    private String trailerUrl;


    private Set<Integer> characterIds;

    private int franchiseId;

    /**
     * Creates a new movieDto object.
     * @param movie Movie object to convert to DTO
     */
    public MovieDto(Movie movie) {
        this.id = movie.getId();
        this.title = movie.getTitle();
        this.genre = movie.getGenre();
        this.releaseYear = movie.getReleaseYear();
        this.director = movie.getDirector();
        this.pictureUrl = movie.getPictureUrl();
        this.trailerUrl = movie.getTrailerUrl();
        setCharacterIds(movie.getCharacters());
        setFranchiseId(movie.getFranchise());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public Set<Integer> getCharacterIds() {
        return characterIds;
    }

    /**
     * Sets the movie characters of the movieDto to a set of integers. If the supplied set of movie characters
     * is empty, sets the field to null. If not, gets the movie character ids from the supplied set and appends to
     * the field set.
     * @param characters set of movie characters to convert and add.
     */
    public void setCharacterIds(Set<MovieCharacter> characters) {
        if (characters == null) {
            this.characterIds = null;
        } else {
            this.characterIds = characters.stream().map(MovieCharacter::getId).collect(Collectors.toSet());
        }
    }

    public int getFranchiseId() {
        return franchiseId;
    }

    /**
     * Sets the movie franchise to an integer instead of a franchise.
     * @param franchise franchise to convert and add.
     */
    public void setFranchiseId(Franchise franchise) {
        if (franchise == null) {
            this.franchiseId = 0;
        } else {
            this.franchiseId = franchise.getId();
        }
    }
}
