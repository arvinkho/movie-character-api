package no.noroff.moviecharactersapi.models.dto.moviecharacter;

/**
 * Represents a Data Transfer Object used for CREATION of a new entity.
 */
public class CreateMovieCharacterDto {
    private String name;
    private String alias;
    private String gender;
    private String pictureUrl;

    public CreateMovieCharacterDto() {
    }

    public CreateMovieCharacterDto(String name, String alias, String gender, String pictureUrl) {
        this.name = name;
        this.alias = alias;
        this.gender = gender;
        this.pictureUrl = pictureUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
