package no.noroff.moviecharactersapi.models.dto.moviecharacter;

import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.domain.MovieCharacter;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents a Data Transfer Object used for getting an existing entity.
 */
public class MovieCharacterDto {

    private int id;
    private String name;
    private String alias;
    private String gender;
    private String pictureUrl;

    // Movie ids
    private Set<Integer> movies;

    /**
     * Creates a new movieCharacterDto object.
     * @param movieCharacter movie character object to convert to DTO
     */
    public MovieCharacterDto(MovieCharacter movieCharacter) {
        this.id = movieCharacter.getId();
        this.name = movieCharacter.getName();
        this.alias = movieCharacter.getAlias();
        this.gender = movieCharacter.getGender();
        this.pictureUrl = movieCharacter.getPictureUrl();
        setMovies(movieCharacter.getMovies());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Set<Integer> getMovies() {
        return movies;
    }

    /**
     * Sets the movies of the movieCharacterDto to a set of integers. If the supplied set of movies is empty,
     * sets the field to null. If not, gets the movie ids from the supplied set and appends to the field set.
     * @param movies set of movies to convert and add.
     */
    public void setMovies(Set<Movie> movies) {
        if (movies == null) {
            this.movies = null;
        } else {
            this.movies = movies.stream().map(Movie::getId).collect(Collectors.toSet());
        }
    }
}
