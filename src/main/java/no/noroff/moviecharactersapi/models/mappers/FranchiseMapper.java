package no.noroff.moviecharactersapi.models.mappers;

import no.noroff.moviecharactersapi.models.domain.Franchise;
import no.noroff.moviecharactersapi.models.dto.franchise.CreateFranchiseDto;
import no.noroff.moviecharactersapi.models.dto.franchise.FranchiseDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class FranchiseMapper implements Mapper<Franchise, FranchiseDto, CreateFranchiseDto>{

    /**
     * Converts from a create-DTO to an entity (domain-object).
     * @param createFranchiseDto create franchise DTO
     * @return franchise entity
     */
    @Override
    public Franchise mapFromCreateDtoToEntity(CreateFranchiseDto createFranchiseDto) {
        Franchise franchise = new Franchise();
        franchise.setName(createFranchiseDto.getName());
        franchise.setDescription(createFranchiseDto.getDescription());
        return franchise;
    }

    /**
     * Converts from franchise entity (domain-object) to franchise DTO
     * @param franchise franchise entity
     * @return franchise DTO
     */
    @Override
    public FranchiseDto mapFromEntityToDto(Franchise franchise) {
        return new FranchiseDto(franchise);
    }

    /**
     * Converts from franchise entity list (domain-object) to franchise DTO list
     * @param franchises franchise entity list
     * @return franchise DTO list
     */
    @Override
    public List<FranchiseDto> mapFromEntityToDtoList(List<Franchise> franchises) {
        List<FranchiseDto> franchiseDtos = new ArrayList<>();
        for (Franchise franchise : franchises) {
            franchiseDtos.add(new FranchiseDto(franchise));
        }
        return franchiseDtos;
    }

    /**
     * Converts from franchise entity set (domain-object) to franchise DTO set
     * @param franchises franchise entity set
     * @return franchise DTO set
     */
    @Override
    public Set<FranchiseDto> mapFromEntityToDtoSet(Set<Franchise> franchises) {
        Set<FranchiseDto> franchiseDtos = new HashSet<>();
        for (Franchise franchise : franchises) {
            franchiseDtos.add(new FranchiseDto(franchise));
        }
        return franchiseDtos;
    }
}
