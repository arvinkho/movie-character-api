package no.noroff.moviecharactersapi.models.mappers;


import java.util.List;
import java.util.Set;

/**
 * This interface is implemented in mapper classes. It converts between entities (domain models),
 * DTOs and Create-DTOs.
 * @param <T> Entity/domain model
 * @param <D> DTO model
 * @param <C> Create-DTO model
 */
public interface Mapper<T, D, C> {
    T mapFromCreateDtoToEntity(C c);
    D mapFromEntityToDto(T t);
    List<D> mapFromEntityToDtoList(List<T> t);
    Set<D> mapFromEntityToDtoSet(Set<T> t);
}
