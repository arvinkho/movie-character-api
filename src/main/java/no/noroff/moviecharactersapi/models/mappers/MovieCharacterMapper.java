package no.noroff.moviecharactersapi.models.mappers;

import no.noroff.moviecharactersapi.models.dto.moviecharacter.CreateMovieCharacterDto;
import no.noroff.moviecharactersapi.models.dto.moviecharacter.MovieCharacterDto;
import no.noroff.moviecharactersapi.models.domain.MovieCharacter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MovieCharacterMapper implements Mapper<MovieCharacter, MovieCharacterDto, CreateMovieCharacterDto>{

    /**
     * Converts from a create-DTO to an entity (domain-object).
     * @param createMovieCharacterDto create movie character DTO
     * @return movie character entity
     */
    @Override
    public MovieCharacter mapFromCreateDtoToEntity(CreateMovieCharacterDto createMovieCharacterDto) {
        MovieCharacter movieCharacter = new MovieCharacter();
        movieCharacter.setName(createMovieCharacterDto.getName());
        movieCharacter.setAlias(createMovieCharacterDto.getAlias());
        movieCharacter.setGender(createMovieCharacterDto.getGender());
        movieCharacter.setPictureUrl(createMovieCharacterDto.getPictureUrl());
        return movieCharacter;
    }

    /**
     * Converts from movie character entity (domain-object) to movie character DTO
     * @param movieCharacter movie character entity
     * @return movie character DTO
     */
    @Override
    public MovieCharacterDto mapFromEntityToDto(MovieCharacter movieCharacter) {
        return new MovieCharacterDto(movieCharacter);
    }

    /**
     * Converts from movie character entity list (domain-object) to movie character DTO list
     * @param movieCharacters movie character entity list
     * @return movie character DTO list
     */
    @Override
    public List<MovieCharacterDto> mapFromEntityToDtoList(List<MovieCharacter> movieCharacters) {
        List<MovieCharacterDto> movieCharacterDtos = new ArrayList<>();
        for (MovieCharacter movieCharacter : movieCharacters) {
            movieCharacterDtos.add(mapFromEntityToDto(movieCharacter));
        }
        return movieCharacterDtos;
    }

    /**
     * Converts from movie character entity set (domain-object) to movie character DTO set
     * @param movieCharacters movie character entity set
     * @return movie character DTO set
     */
    @Override
    public Set<MovieCharacterDto> mapFromEntityToDtoSet(Set<MovieCharacter> movieCharacters) {
        Set<MovieCharacterDto> movieCharacterDtos = new HashSet<>();
        for (MovieCharacter movieCharacter : movieCharacters) {
            movieCharacterDtos.add(mapFromEntityToDto(movieCharacter));
        }
        return movieCharacterDtos;
    }
}
