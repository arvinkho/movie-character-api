package no.noroff.moviecharactersapi.models.mappers;

import no.noroff.moviecharactersapi.models.dto.movie.CreateMovieDto;
import no.noroff.moviecharactersapi.models.dto.movie.MovieDto;
import no.noroff.moviecharactersapi.models.domain.Movie;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MovieMapper implements Mapper<Movie, MovieDto, CreateMovieDto> {


    /**
     * Converts from a movie create-DTO to a movie entity (domain-object).
     * @param createMovieDto create movie DTO
     * @return movie entity
     */
    @Override
    public Movie mapFromCreateDtoToEntity(CreateMovieDto createMovieDto) {
        Movie movie = new Movie();
        movie.setTitle(createMovieDto.getTitle());
        movie.setGenre(createMovieDto.getGenre());
        movie.setReleaseYear(createMovieDto.getReleaseYear());
        movie.setDirector(createMovieDto.getDirector());
        movie.setPictureUrl(createMovieDto.getPictureUrl());
        movie.setTrailerUrl(createMovieDto.getTrailerUrl());
        return movie;
    }

    /**
     * Converts from movie entity (domain-object) to movie DTO
     * @param movie movie entity
     * @return movie DTO
     */
    @Override
    public MovieDto mapFromEntityToDto(Movie movie) {
        return new MovieDto(movie);
    }

    /**
     * Converts from movie entity list (domain-object) to movie DTO list
     * @param movies movie entity list
     * @return movie DTO list
     */
    @Override
    public List<MovieDto> mapFromEntityToDtoList(List<Movie> movies) {
        List<MovieDto> movieDtos = new ArrayList<>();
        for (Movie movie : movies) {
            movieDtos.add(mapFromEntityToDto(movie));
        }
        return movieDtos;
    }

    /**
     * Converts from movie entity set (domain-object) to movie DTO set
     * @param movies movie entity set
     * @return movie DTO set
     */
    @Override
    public Set<MovieDto> mapFromEntityToDtoSet(Set<Movie> movies) {
        Set<MovieDto> movieDtos = new HashSet<>();
        for (Movie movie : movies) {
            movieDtos.add(mapFromEntityToDto(movie));
        }
        return movieDtos;
    }
}
