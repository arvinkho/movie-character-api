package no.noroff.moviecharactersapi.services;

import no.noroff.moviecharactersapi.models.domain.Franchise;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.domain.MovieCharacter;
import no.noroff.moviecharactersapi.models.mappers.FranchiseMapper;
import no.noroff.moviecharactersapi.repositories.FranchiseRepository;
import no.noroff.moviecharactersapi.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a movie character service. This class is injectable.
 *
 * Handles the business logic between the repository and the controller.
 */
@Service
public class FranchiseService {

    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;

    /**
     * Get all the movies in a given franchise by franchise id.
     * @param id franchise id
     * @return all movies in the franchise
     */
    public Set<Movie> getAllMoviesById(int id) {
        return franchiseRepository.getById(id).getMovies();
    }

    /**
     * Get all movie characters from a single franchise by franchise id.
     * @param id franchise id
     * @return all the characters in the franchise
     */
    public Set<MovieCharacter> getAllMovieCharactersById(int id) {
        Set<MovieCharacter> movieCharacters = new HashSet<>();
        for (Movie movie : getAllMoviesById(id)) {
            movieCharacters.addAll(movie.getCharacters());
        }
        return movieCharacters;
    }

    /**
     * Checks if the franchise with the given id exists in the database
     * @param id franchise id
     * @return true if it exists, false if not
     */
    public boolean existsById(int id) {
        return franchiseRepository.existsById(id);
    }

    /**
     * Get all franchises from the database
     * @return all franchises present in the database
     */
    public List<Franchise> getAll() {
        return franchiseRepository.findAll();
    }

    /**
     * Get a single franchise from the database by franchise id
     * @param id franchise id
     * @return franchise
     */
    public Franchise getById(int id) {
        return franchiseRepository.getById(id);
    }

    /**
     * Save the given franchise to the database
     * @param franchise franchise to save to the database
     * @return the saved franchise
     */
    public Franchise save(Franchise franchise) {
        return franchiseRepository.save(franchise);
    }

    /**
     * Delete a franchise from the database based on franchise id.
     * It performs a check to see if the franchise contains any movies, and
     * removes these if it does. Finally, the franchise will be deleted from the database.
     * @param id franchise id to delete
     * @return franchise id
     */
    public int delete(int id) {
        if (!franchiseRepository.getById(id).getMovies().isEmpty()) {
            List<Movie> movies = new ArrayList<>();
            for (Movie currentMovie : franchiseRepository.getById(id).getMovies()) {
                currentMovie.setFranchise(null);
                movies.add(currentMovie);
            }
            movieRepository.saveAll(movies);
        }
        franchiseRepository.deleteById(id);
        return id;
    }


    /**
     * Updates the movies in the franchise. Gets the franchise by id, and fills this franchise with the
     * movies corresponding to the given movie ids. Finally, save the newly updated franchise to the database
     * @param franchiseId franchise id to update
     * @param movieIds movie ids to add to the franchise
     * @return the newly updated franchise
     */
    public Franchise updateMovies(int franchiseId, List<Integer> movieIds) {
        Franchise franchise = franchiseRepository.getById(franchiseId);
        List<Movie> movies = new ArrayList<>();
        for (Integer movieId : movieIds) {
            if (movieRepository.existsById(movieId)) {
                Movie currentMovie = movieRepository.getById(movieId);
                currentMovie.setFranchise(franchise);
                movies.add(currentMovie);
            }
        }
        movieRepository.saveAll(movies);
        return franchiseRepository.getById(franchiseId);
    }


    /**
     * Returns the number of entities available.
     * @return the number of entities
     */
    public long count() {
        return franchiseRepository.count();
    }

}
