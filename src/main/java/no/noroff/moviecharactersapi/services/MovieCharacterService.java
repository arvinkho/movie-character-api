package no.noroff.moviecharactersapi.services;

import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.domain.MovieCharacter;
import no.noroff.moviecharactersapi.repositories.MovieCharacterRepository;
import no.noroff.moviecharactersapi.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Represents a movie character service. This class is injectable.
 *
 * Handles the business logic between the repository and the controller.
 */
@Service
public class MovieCharacterService {
    @Autowired
    private MovieCharacterRepository movieCharacterRepository;
    @Autowired
    private MovieRepository movieRepository;

    /**
     * Get all the movies the current movie character is in, based on movie
     * character id.
     * @param id movie character id
     * @return the movies the movie character is a part of
     */
    public Set<Movie> getMoviesFromMovieCharacter(int id) {
        return movieCharacterRepository.getById(id).getMovies();
    }

    /**
     * Checks if the movie character with the given id exists in the database
     * @param id movie character id
     * @return true if it exists, false if not
     */
    public boolean existsById(int id) {
        return movieCharacterRepository.existsById(id);
    }

    /**
     * Get all movie characters in the database
     * @return all moviecharacters in the database
     */
    public List<MovieCharacter> getAll() {
        return movieCharacterRepository.findAll();
    }

    /**
     * Get a single movie character by id
     * @param id movie character id
     * @return movie character
     */
    public MovieCharacter getById(int id) {
        return movieCharacterRepository.getById(id);
    }

    /**
     * Save the given movie character to the database
     * @param movieCharacter movie character to save
     * @return the saved movie character
     */
    public MovieCharacter save(MovieCharacter movieCharacter) {
        return movieCharacterRepository.save(movieCharacter);
    }

    /**
     * Delete a single movie character by id from the database
     * @param id movie character id to delete
     * @return movie character id
     */
    public int delete(int id) {
        if (movieCharacterRepository.getById(id).getMovies() != null) {
            Set<Movie> movies = movieCharacterRepository.getById(id).getMovies();
            Set<Movie> updatedMovies = new HashSet<>();
            for (Movie movie : movies) {
                Set<MovieCharacter> currentCharacters = movie.getCharacters();
                Iterator<MovieCharacter> it = currentCharacters.iterator();
                while (it.hasNext()) {
                    MovieCharacter currentCharacter = it.next();
                    if (currentCharacter.equals(movieCharacterRepository.getById(id))) {
                        it.remove();
                        break;
                    }
                }
                movie.setCharacters(currentCharacters);
                updatedMovies.add(movie);
            }
            movieRepository.saveAll(updatedMovies);
        }
        movieCharacterRepository.deleteById(id);
        return id;
    }

    /**
     * Returns the number of entities available.
     * @return the number of entities
     */
    public long count() {
        return movieCharacterRepository.count();
    }



}

