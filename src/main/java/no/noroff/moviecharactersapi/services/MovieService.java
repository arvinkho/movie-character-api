package no.noroff.moviecharactersapi.services;

import no.noroff.moviecharactersapi.models.mappers.MovieCharacterMapper;
import no.noroff.moviecharactersapi.models.dto.moviecharacter.MovieCharacterDto;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.domain.MovieCharacter;
import no.noroff.moviecharactersapi.repositories.FranchiseRepository;
import no.noroff.moviecharactersapi.repositories.MovieCharacterRepository;
import no.noroff.moviecharactersapi.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a movie service. This class is injectable.
 *
 * Handles the business logic between the repository and the controller.
 */
@Service
public class MovieService {


    @Autowired
    private MovieCharacterRepository movieCharacterRepository;
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieCharacterMapper movieCharacterMapper;


    /**
     * Get all movie characters from one single movie, based on movie id.
     * @param id id of the movie
     * @return movie characters in the movie.
     */
    public Set<MovieCharacter> getMovieCharactersFromMovie(int id) {
        return movieRepository.getById(id).getCharacters();
    }

    /**
     * Checks if the movie with the given id exists in the database
     * @param id movie id
     * @return true if it exists, false if not
     */
    public boolean existsById(int id) {
        return movieRepository.existsById(id);
    }

    /**
     * Get all movies from the database
     * @return all movies from the database
     */
    public List<Movie> getAll() {
        return movieRepository.findAll();
    }

    /**
     * Get a single movie by movie id
     * @param id movie id
     * @return movie
     */
    public Movie getById(int id) {
        return movieRepository.getById(id);
    }

    /**
     * Save the given movie to the database
     * @param movie movie to save
     * @return the saved movie
     */
    public Movie save(Movie movie) {
        return movieRepository.save(movie);
    }

    /**
     * Delete a given movie by movie id
     * @param id movie id to delete
     * @return movie id
     */
    public int delete(int id) {
        movieRepository.deleteById(id);
        return id;
    }

    /**
     * Update the characters in the movie supplied by movieID.
     * It will find the movie with the correct movie ID in the database,
     * and find all corresponding characters based on the supplied list, and
     * add these characters to the movie. Finally, it will save this new movie in the database.
     * @param movieId movie id of the movie to update
     * @param characterIds array of character ids to add
     * @return the updated and saved movie object
     */
    public Movie updateCharacters(int movieId, List<Integer> characterIds) {
        Movie movie = movieRepository.getById(movieId);
        Set<MovieCharacter> characters = new HashSet<>();
        for (Integer characterId : characterIds) {
            MovieCharacter currentCharacter = movieCharacterRepository.getById(characterId);
            characters.add(currentCharacter);
        }
        movie.setCharacters(characters);
        return save(movie);
    }

    /**
     * Returns the number of entities available.
     * @return the number of entities
     */
    public long count() {
        return movieRepository.count();
    }


}
