package no.noroff.moviecharactersapi.utils;

import no.noroff.moviecharactersapi.models.domain.Franchise;
import no.noroff.moviecharactersapi.models.dto.franchise.CreateFranchiseDto;
import no.noroff.moviecharactersapi.models.mappers.FranchiseMapper;
import no.noroff.moviecharactersapi.models.mappers.MovieCharacterMapper;
import no.noroff.moviecharactersapi.models.mappers.MovieMapper;
import no.noroff.moviecharactersapi.models.dto.movie.CreateMovieDto;
import no.noroff.moviecharactersapi.models.dto.moviecharacter.CreateMovieCharacterDto;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.domain.MovieCharacter;
import no.noroff.moviecharactersapi.services.FranchiseService;
import no.noroff.moviecharactersapi.services.MovieCharacterService;
import no.noroff.moviecharactersapi.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Represents a data seeder, it checks if the database tables are empty, and creates dummy data for
 * franchises, movies and movie characters.
 */
@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    private MovieService movieService;
    @Autowired
    private MovieCharacterService movieCharacterService;
    @Autowired
    private FranchiseService franchiseService;

    @Autowired
    private MovieMapper movieMapper;
    @Autowired
    private MovieCharacterMapper movieCharacterMapper;
    @Autowired
    private FranchiseMapper franchiseMapper;

    @Override
    public void run(ApplicationArguments args){
        loadMovieCharacterData();
        loadFranchises();
        loadMovies();
    }

    /**
     * If the movie character table of the DB is empty, create dummy
     * movie characters and fill into the movie character table.
      */
    private void loadMovieCharacterData() {
        if (movieCharacterService.count() == 0) {
            MovieCharacter movieCharacter1 = movieCharacterMapper.mapFromCreateDtoToEntity(new CreateMovieCharacterDto(
                    "Harry Potter",
                    "The boy who lived",
                    "male",
                    "https://static.wikia.nocookie.net/harrypotter/images/d/d4/Dhharryroomhighreso.jpg/revision/latest/scale-to-width-down/250?cb=20130331232335&path-prefix=no"
            ));
            MovieCharacter movieCharacter2 = movieCharacterMapper.mapFromCreateDtoToEntity(new CreateMovieCharacterDto(
                    "Voldemort",
                    "Tom Riddle",
                    "male",
                    "https://static.wikia.nocookie.net/harrypotter/images/1/1c/Tomdh.jpg/revision/latest?cb=20130315210914&path-prefix=no"
            ));
            MovieCharacter movieCharacter3 = movieCharacterMapper.mapFromCreateDtoToEntity(new CreateMovieCharacterDto(
                    "James Bond",
                    "007",
                    "male",
                    "https://static.wikia.nocookie.net/jamesbond/images/d/dc/James_Bond_%28Pierce_Brosnan%29_-_Profile.jpg/revision/latest?cb=20220207082851"
            ));
            MovieCharacter movieCharacter4 = movieCharacterMapper.mapFromCreateDtoToEntity(new CreateMovieCharacterDto(
                    "Ernst Stavro Blofeld",
                    "Dr. Guntram von Shatterhand",
                    "male",
                    "https://i1.sndcdn.com/artworks-000031407928-ivngho-t500x500.jpg"
            ));
            MovieCharacter movieCharacter5 = movieCharacterMapper.mapFromCreateDtoToEntity(new CreateMovieCharacterDto(
                    "Anakin Skywalker",
                    "Darth Vader",
                    "male",
                    "https://www.sølvberget.no/var/news_site/storage/images/stavanger-bibliotek/lesetips/hvordan-ville-lukes-bok-om-darth-vader-blitt/292006-1-nor-NO/Hvordan-ville-Lukes-bok-om-Darth-Vader-blitt_mainarticlethumbnail.jpg"
            ));
            MovieCharacter movieCharacter6 = movieCharacterMapper.mapFromCreateDtoToEntity(new CreateMovieCharacterDto(
                    "Obi-Wan Kenobi",
                    "Ben Kenobi",
                    "male",
                    "https://upload.wikimedia.org/wikipedia/en/3/32/Ben_Kenobi.png"
            ));
            movieCharacterService.save(movieCharacter1);
            movieCharacterService.save(movieCharacter2);
            movieCharacterService.save(movieCharacter3);
            movieCharacterService.save(movieCharacter4);
            movieCharacterService.save(movieCharacter5);
            movieCharacterService.save(movieCharacter6);
        }
    }


    /**
     * If the movie table of the DB is empty, create dummy movies and
     * fill into the movie table. It will also bind franchises and movie characters
     * to the movies.
     */
    private void loadMovies() {
        if (movieService.count() == 0) {
            Movie movie1 = movieMapper.mapFromCreateDtoToEntity(new CreateMovieDto(
                    "Harry Potter and the Deathly Hallows part 1",
                    "Fantasy",
                    2010,
                    "David Yates",
                    "https://static1.colliderimages.com/wordpress/wp-content/uploads/2021/11/hp7.jpg?q=50&fit=contain&w=767&h=384&dpr=1.5",
                    "https://www.youtube.com/watch?v=MxqsmsA8y5k"
            ));
            Movie movie2 = movieMapper.mapFromCreateDtoToEntity(new CreateMovieDto(
                    "James Bond: From Russia with Love",
                    "Spy film",
                    1963,
                    "Terence Yound",
                    "https://upload.wikimedia.org/wikipedia/en/a/ad/From_Russia_with_Love_–_UK_cinema_poster.jpg?20121111111243",
                    "https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiDm7bjuqn2AhXZSvEDHXw_CtIQyCl6BAgDEAM&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Dt9AeIdMQqR8&usg=AOvVaw1FdYHcm-vwIG8U7wyMSLtt"
            ));
            Movie movie3 = movieMapper.mapFromCreateDtoToEntity(new CreateMovieDto(
                    "Star Wars Episode V: The Empire Strikes Back",
                    "Epic Space Opera",
                    1980,
                    "Irvin Kershner",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOuzmfczWOiCZEr2MMCA2NzsxwfHzaqhP7_bSlEO1uh6d9Ysuw",
                    "https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjf7bCau6n2AhUORfEDHcTDDWQQyCl6BAgDEAM&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DJNwNXF9Y6kY&usg=AOvVaw0zkNe5vvJ_LX4FCXEOyi9c"
            ));
            Set<MovieCharacter> movieCharacters1 = new HashSet<>();
            movieCharacters1.add(movieCharacterService.getById(1));
            movieCharacters1.add(movieCharacterService.getById(2));
            movie1.setCharacters(movieCharacters1);
            movie1.setFranchise(franchiseService.getById(1));

            Set<MovieCharacter> movieCharacters2 = new HashSet<>();
            movieCharacters2.add(movieCharacterService.getById(3));
            movieCharacters2.add(movieCharacterService.getById(4));
            movie2.setCharacters(movieCharacters2);
            movie2.setFranchise(franchiseService.getById(2));

            Set<MovieCharacter> movieCharacters3 = new HashSet<>();
            movieCharacters3.add(movieCharacterService.getById(5));
            movieCharacters3.add(movieCharacterService.getById(6));
            movie3.setCharacters(movieCharacters3);
            movie3.setFranchise(franchiseService.getById(3));

            movieService.save(movie1);
            movieService.save(movie2);
            movieService.save(movie3);
        }
    }


    /**
     * If the franchise table of the DB is empty, create dummy franchises and
     * fill into the franchise table.
     */
    private void loadFranchises() {
        if (franchiseService.count() == 0) {
            Franchise franchise1 = franchiseMapper.mapFromCreateDtoToEntity(new CreateFranchiseDto(
                    "The Harry Potter Series",
                    "Blablabla"
            ));
            Franchise franchise2 = franchiseMapper.mapFromCreateDtoToEntity(new CreateFranchiseDto(
                    "The James Bond Collection",
                    "Spies and shit"
            ));
            Franchise franchise3 = franchiseMapper.mapFromCreateDtoToEntity(new CreateFranchiseDto(
                    "The Star Wars Saga",
                    "Aliens and laser swords and stuff. Pew pew"
            ));

            franchiseService.save(franchise1);
            franchiseService.save(franchise2);
            franchiseService.save(franchise3);
        }
    }
}
